# Taskie

Taskie is a wrapper for taskwarrior built with Gum that allows the user to easily select one or more projects and tags to add to the task automatically without having to type everything out manually. With Taskie, you can quickly create tasks, assign them to projects and tags, and set due dates and priorities/effort level. Taskie integrates seamlessly with taskwarrior, allowing you to type less and do more. Whether you're a busy professional, a student, or just someone who wants to stay organized, Taskie is the perfect tool for managing your tasks efficiently and effectively.

## Requirements

This program requires the dependency Gum from [Charm.sh](https://github.com/charmbracelet/gum) to be installed.
