#!/bin/bash

# Get the project(s) to add to the task
PROJECT=$(task _unique project | gum choose --no-limit)
clear

# Get list of unique tags, choose tags to use, then append a plus to the tags chosen
# using sed (to make them usable with taskwarrior)
TAGS=$(task _unique tag | gum choose --no-limit | sed 's/^/+/;s/\ /\ +/')
clear

# Get due date
DUE=$(gum input --prompt=">>>: " --placeholder="Due date, today? [Y/n]")

# Set the due date depending on user input
case "$DUE" in
  [yY]) DUE="eod" ;;
  [nN]) clear && DUE=$(gum input --prompt=">>>: " --placeholder="Enter due date (YYYY-MM-DD)") ;;
  *) DUE="" ;;
esac
clear

# Get the task name
TASK_NAME=$(gum input --prompt=">>>: " --placeholder="Enter task name")
clear

# Create the task
printf -v ARGS "project:%s %s due:%s %s\n" "$PROJECT" "$TAGS" "$DUE" "$TASK_NAME"
task add $(echo "$ARGS")